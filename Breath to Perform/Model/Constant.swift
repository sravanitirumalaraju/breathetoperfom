//
//  Constant.swift
//  Breath to Perform
//
//  Created by apple on 12/26/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation

var BaseUrl = "https://breathe.courageisaskill.com/"

var signUpURL = BaseUrl + "signUp"
var loginURL = BaseUrl + "login"
var getUserDetailsURL = BaseUrl + "api/EditUserDetails/"
var forgetPasswordURL = BaseUrl + "forgetPassword"
var userForgotURL = BaseUrl + "api/logout/"
var appRegisterationURL = BaseUrl + "api/AppRegistration"
var deleteRegisterationTokenURL = BaseUrl + "api/DeleteRegisterTokenOnSignout"
var getAllCourseListURL = BaseUrl + "api/GetAllCourseList"
var getCourseDetailsURL = BaseUrl + "api/GetCourseDetails/"
var getExerciseListURL = BaseUrl + "api/GetChallengesListByCourse/"
var getAllChallengesListURL = BaseUrl + "api/GetAllChallengeList"
var getUserPaymentDetailsURL = BaseUrl + "api/UserCoursePaymentDetails/"
var addChallengeStatusURL = BaseUrl + "api/addChallengeStatus"
var getChallengeStatusURL = BaseUrl + "api/GetChallengeStatus/"
var deleteTokenURL = BaseUrl + "api/DeleteRegisterTokenOnSignout"
var getBackgroundMusicURL = BaseUrl + "api/GetAllBackgroundMusics"
var updatePaymentURL = BaseUrl + "api/UpdateUserPayment"
