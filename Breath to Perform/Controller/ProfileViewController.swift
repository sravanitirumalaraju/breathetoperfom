//
//  ProfileViewController.swift
//  Breath to Perform
//
//  Created by apple on 11/1/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProfileViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate  {
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userImageButton: UIButton!
    @IBOutlet weak var userNameTxtFld: UITextField!
    @IBOutlet weak var emailTxtFld: BreatheUITextField!
    @IBOutlet weak var mobileNoTxtFld: BreatheUITextField!
    @IBOutlet weak var editBtn: UIButton!
//    var userId = String()
    var imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        userImageView.layer.cornerRadius = userImageView.frame.size.width / 2
        userImageView.layer.masksToBounds = true

        let userEmailId = UserDefaults.standard.value(forKey: "emailId")
        fetchUserDetails(emailId: userEmailId as! String)
    }
    
    func fetchUserDetails(emailId:String){
        if NetworkingManager.isConnectedToNetwork(){
            showActivityIndicator()
            //retrieve the token and add it to header
            let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
            let tokenString = "Bearer " + retrievedToken!
            let userHeaders: HTTPHeaders = ["Authorization": tokenString, "Content-Type": "application/json"]
            Alamofire.request(getUserDetailsURL + emailId, headers: userHeaders).responseString { response in
                switch(response.result) {
                case .success:
                    let queryData = self.convertToDictionary(text: response.result.value!)
                    let userResult = queryData!["result"] as! NSDictionary
                    self.emailTxtFld.text = userResult["emailId"] as? String
                    self.mobileNoTxtFld.text = userResult["mobileNumber"] as? String
                    self.userNameTxtFld.text = userResult["userName"] as? String
                    if let userImageString = userResult["userImage"] as? String{
                        let url = URL(string: userImageString)
                        let data = try? Data(contentsOf: url!)
                        self.userImageView.image = UIImage(data: data!)
                    }
                    hideActivityIndicator()
                case .failure(_):
                    hideActivityIndicator()
                    print(response.result.error!)
                    break
                }
                debugPrint(response)
            }
        }else{
            print("no internet")
            Alerts.alertBox(Mymsg: "no internet connection", view: self)
        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    @IBAction func userImageTapped(_ sender: Any) {
        let actionSheetController: UIAlertController = UIAlertController (title: "Choose an Option", message: nil, preferredStyle: .actionSheet)
        actionSheetController.addAction( UIAlertAction (title: "Cancel", style: .cancel, handler: nil))
        actionSheetController.addAction( UIAlertAction (title: "Photo Library", style: .default, handler:{ (alert: UIAlertAction!) in self.chooseFromPhotoLibrary()}
        ))
        actionSheetController.addAction( UIAlertAction (title: "Take Picture", style: .default, handler: {(alert: UIAlertAction!) in self.chooseFromCamera()}
        ))
        self.view.endEditing(true)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    func chooseFromCamera() {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.cameraCaptureMode = .photo
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    func chooseFromPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var selectedImage: UIImage?
        userImageView.contentMode = .scaleAspectFit
        selectedImage = info[UIImagePickerControllerEditedImage] as? UIImage
        self.userImageView.image = selectedImage
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func backClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signOutClicked(_ sender: Any) {
        if NetworkingManager.isConnectedToNetwork(){
            let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
            let tokenString = "Bearer " + retrievedToken!
            let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
            Alamofire.request(deleteTokenURL, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: Headers).responseJSON{ response in
                print(response)
                switch (response.result){
                case .success(_):
                    let destVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    self.navigationController?.pushViewController(destVc, animated: true)
                    UserDefaults.standard.set("", forKey: "token")
                    UserDefaults.standard.synchronize()
                    break
                case .failure(_):
                    print(response.result.error!)
                    break
                }
            }
        }else{
            Alerts.alertBox(Mymsg: "no internet connection", view: self)
        }
    }
    
}
