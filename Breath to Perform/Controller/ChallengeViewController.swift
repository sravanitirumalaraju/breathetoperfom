//
//  ChallengeViewController.swift
//  Breath to Perform
//
//  Created by apple on 11/1/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import MediaPlayer
import AVFoundation
import SwiftyGif
import Alamofire
import SwiftyJSON
import AVKit

class ChallengeViewController: UIViewController,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,AVAudioPlayerDelegate {

    @IBOutlet weak var tintLabel: UILabel!
    @IBOutlet weak var MarkAsCompletedBtn: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var breatheImage: UIImageView!
    @IBOutlet weak var titleChallengeLbl: UILabel!
    @IBOutlet weak var forwardTimerLbl: UILabel!
    @IBOutlet weak var backwardTimerLbl: UILabel!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var selectbackgrndBtn: UIButton!
    @IBOutlet weak var backgroundMusicTableVw: UITableView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var playImageView: UIImageView!
    
    @IBOutlet weak var audioDropdownImg: UIImageView!
    @IBOutlet weak var selectAudioTitleLbl: UILabel!
    var audioPlayer:AVAudioPlayer!
//    var backgroundPlayer:AVAudioPlayer!
    var isAudioPlayed = Bool()
    var challengeDetailsDict = NSDictionary()
    var blurView = UIView()
    
    var backgroundArray = Array<Any>()
    var exerciseNumber = String()
    var audioResourceName = String()
    var numberLabelString = String()
    
    var descriptionLbl = UILabel()
    var readMoreBtn = UIButton()
    var LabelHeight = UILabel()
    
    var nextChallengeIdString = String()
    var unlockedChallengeIdString = String()
    
    var linkButton = UIButton()
    let linkLbl = UILabel()
    var challengeAlreadyCompleted: Bool = false
    var challengePath = "https://d38p67sopieddf.cloudfront.net/"
    var purchaseInfoDict = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        breatheImage.layer.cornerRadius = breatheImage.frame.size.height/2
        breatheImage.clipsToBounds = true
//        let gif = UIImage(gifName: "nature.gif")
//        self.backgroundImage.setGifImage(gif)
//        audioPlayer.delegate = self
        print(challengeDetailsDict)
        backgroundArray = (challengeDetailsDict["challengeBgm"] as! Array<Any>)
//        if backgroundArray.count == 0{
//            selectAudioTitleLbl.isHidden = true
//            audioDropdownImg.isHidden = true
//            selectbackgrndBtn.isHidden = true
//        }
        
        MarkAsCompletedBtn.layer.cornerRadius = MarkAsCompletedBtn.frame.size.height/2
        MarkAsCompletedBtn.clipsToBounds = true
        
        selectbackgrndBtn.layer.cornerRadius = selectbackgrndBtn.frame.size.height/2
        selectbackgrndBtn.clipsToBounds = true

        linkLbl.isHidden = true
        linkButton.isHidden = true

        descriptionLbl.frame = CGRect(x: 0, y: 10, width: 300, height: 65)
        descriptionLbl.numberOfLines = 0
        descriptionLbl.center.x = self.view.center.x
        descriptionLbl.backgroundColor = UIColor.clear
        descriptionLbl.textAlignment = .center
        descriptionLbl.textColor = UIColor.white
        descriptionLbl.text = challengeDetailsDict["description"] as? String
        descriptionLbl.font = UIFont(name:"HelveticaNeue-Bold", size: 17.0)
        scrollView.addSubview(descriptionLbl)
        
        LabelHeight.frame = CGRect(x: 0, y: 10, width: 300, height: LabelHeight.frame.height)
        LabelHeight.text = challengeDetailsDict["description"] as? String
        LabelHeight.font = UIFont(name:"HelveticaNeue-Bold", size: 17.0)
        LabelHeight.numberOfLines = 0
        LabelHeight.sizeToFit()
        LabelHeight.center.x = self.view.center.x
        LabelHeight.textAlignment = .center
        
        readMoreBtn.isHidden = true
        
        blurView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        blurView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.view.addSubview(blurView)
        blurView.isHidden = true
        
        if let audioURLString = challengeDetailsDict["challengeFilePath"] as? String{
            let audioFilePath = "\(challengePath)\(String(describing: audioURLString))"
            let url = NSURL(string: audioFilePath)
            downloadFileFromURL(url: url!)
        }else{
            Alerts.alertBox(Mymsg: "No audio file found", view: self)
        }
//        titleChallengeLbl.text = "Breathe to Perform"
//        titleChallengeLbl.text = challengeDetailsDict["challengeName"] as? String
        
//        if (challengeDetailsDict["challengeImage"] as! String) != "https://d38p67sopieddf.cloudfront.net/challengesimages/course-BRAVE/undefined"{
//            if let imageURLString = challengeDetailsDict["challengeImage"] as? String {
//                let url = URL(string: imageURLString)
//                let data = try? Data(contentsOf: url!)
//                self.challengeImage.image = UIImage(data: data!)
//            }
//        }
        
        if LabelHeight.frame.size.height > scrollView.frame.size.height{
            descriptionLbl.frame = CGRect(x: 0, y: 10, width: 300, height: scrollView.frame.size.height - 20)
            
            readMoreBtn.isHidden = false
            readMoreBtn.frame = CGRect(x: 0, y: descriptionLbl.frame.origin.y + descriptionLbl.frame.size.height, width: 100, height: 20)
            readMoreBtn.setTitle("Read more", for: .normal)
            readMoreBtn.center.x = self.view.center.x
            readMoreBtn.backgroundColor = UIColor.clear
            readMoreBtn.setTitleColor(UIColor(red: 35.0/255.0, green: 85.0/255.0, blue: 135.0/255.0, alpha: 1.0), for: .normal)
            readMoreBtn.addTarget(self, action: #selector(readMore_tapped(_:)), for: .touchUpInside)
            scrollView.addSubview(readMoreBtn)
        }else{
            descriptionLbl.frame = CGRect(x: 0, y: 10, width: 300, height: LabelHeight.frame.height)
            if let linkURLString = challengeDetailsDict["challengeUrl"] as? String {
                linkLbl.isHidden = false
                linkButton.isHidden = false
                linkLbl.frame = CGRect(x: 0, y: descriptionLbl.frame.size.height + descriptionLbl.frame.origin.y + 5, width: 300, height: linkLbl.frame.height)
                linkLbl.text = linkURLString
                linkLbl.numberOfLines = 0
                linkLbl.textColor = UIColor(red: 35.0/255.0, green: 85.0/255.0, blue: 135.0/255.0, alpha: 1.0)
                linkLbl.textAlignment = .center
                linkLbl.sizeToFit()
                linkLbl.center.x = self.view.center.x
                scrollView.addSubview(linkLbl)
                
                linkButton.frame = CGRect(x: 0, y: descriptionLbl.frame.size.height + descriptionLbl.frame.origin.y + 2, width: 300, height: linkLbl.frame.size.height)
                linkButton.center.x = self.view.center.x
//                linkButton.addTarget(self, action: #selector(linkClicked), for: .touchUpInside)
                scrollView.addSubview(linkButton)
            }
        }
        descriptionLbl.center.x = self.view.center.x
        descriptionLbl.textAlignment = .center

        var contentRect = CGRect.zero
        for view in scrollView.subviews {
            contentRect = contentRect.union(view.frame)
        }
        scrollView.contentSize.height = contentRect.size.height
        
        backgroundMusicTableVw.delegate = self
        backgroundMusicTableVw.dataSource = self
        backgroundMusicTableVw.register(UINib(nibName: "MusicTableViewCell", bundle: nil), forCellReuseIdentifier: "MusicTableViewCell")
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tintLabel.addGestureRecognizer(tap)
        tintLabel.isUserInteractionEnabled = true
    }
    
    //MARK:- HANDLE TAP GESTURE
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        backgroundMusicTableVw.isHidden = true
    }
    func downloadFileFromURL(url:NSURL){
        showActivityIndicator()
        var downloadTask:URLSessionDownloadTask
        downloadTask = URLSession.shared.downloadTask(with: url as URL, completionHandler: { [weak self](URL, response, error) -> Void in
            do {
                if URL != nil{
                    try self?.audioPlayer = AVAudioPlayer(contentsOf: URL!)
                    self?.audioPlayer.delegate = self
                    DispatchQueue.main.async {
                        hideActivityIndicator()
                        self?.forwardTimerLbl.text = self?.timeString(time: TimeInterval((self?.audioPlayer.duration)!))
                    }
                }else{
                    DispatchQueue.main.async {
                        hideActivityIndicator()
                    }
                    Alerts.alertBox(Mymsg: "No audio found", view: self!)
                }
            }catch {
                DispatchQueue.main.async {
                    hideActivityIndicator()
                }
                Alerts.alertBox(Mymsg: "Audio file is not in right format", view: self!)
            }
        })
        downloadTask.resume()
    }
    override func viewWillAppear(_ animated: Bool) {
        checkIfChallengeCompleted()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return backgroundArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let MusicTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MusicTableViewCell", for: indexPath) as! MusicTableViewCell
        MusicTableViewCell.selectionStyle = .none
        let backgroundDict = backgroundArray[indexPath.row] as! NSDictionary
        MusicTableViewCell.backgroundNameLbl.text = backgroundDict["title"] as? String
        if backgroundDict["status"] as? String == "FREE" || (UserDefaults.standard.value(forKey: "payUpdate") as? String == "YES"){
            MusicTableViewCell.lockBtn.isHidden = true
        }else{
            MusicTableViewCell.lockBtn.isHidden = false
        }
        return MusicTableViewCell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        audioPlayer.stop()
        let backgroundDict = backgroundArray[indexPath.row] as! NSDictionary
        if backgroundDict["status"] as? String == "FREE" || (UserDefaults.standard.value(forKey: "payUpdate") as? String == "YES"){
            backgroundMusicTableVw.isHidden = true
            audioResourceName = backgroundDict["bgmUrl"] as! String
            isAudioPlayed = false
            downloadBackgroundMusic()
        }else{
            self.alertBox(myMsg: "Join Breathe to Perform Pro to unlock all musical selections")
        }
    }
    //MARK:- ALERTS FOR COURSES
    func alertBox (myMsg: String){
        let alert = UIAlertController(title: "", message: myMsg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "PAY", style: .default, handler: { UIAlertAction in
            let destVc = self.storyboard?.instantiateViewController(withIdentifier: "PurchaseViewController") as! PurchaseViewController
            destVc.courseDetailsDict = self.purchaseInfoDict
            self.navigationController?.pushViewController(destVc, animated: true)
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    func downloadBackgroundMusic(){
        showActivityIndicator()
        var downloadTask:URLSessionDownloadTask
        let backgroundUrl = NSURL(string: audioResourceName)
        downloadTask = URLSession.shared.downloadTask(with: backgroundUrl! as URL, completionHandler: { [weak self](URL, response, error) -> Void in
            do {
                if URL != nil{
                    try self?.audioPlayer = AVAudioPlayer(contentsOf: URL!)
                    self?.audioPlayer.delegate = self
                    DispatchQueue.main.async {
                        hideActivityIndicator()
                        self?.forwardTimerLbl.text = self?.timeString(time: TimeInterval((self?.audioPlayer.duration)!))
                        self!.playClicked(self!)
                    }
                }else{
                    DispatchQueue.main.async {
                        hideActivityIndicator()
                    }
                    Alerts.alertBox(Mymsg: "No audio found", view: self!)
                }
            }catch {
                DispatchQueue.main.async {
                    hideActivityIndicator()
                }
                Alerts.alertBox(Mymsg: "Audio file is not in right format", view: self!)
            }
        })
        downloadTask.resume()
    }
    @IBAction func backgrndMusicClicked(_ sender: Any) {
        if backgroundArray.count == 0{
            if UserDefaults.standard.value(forKey: "payUpdate") as? String == "YES"{
                Alerts.alertBox(Mymsg: "musical files are not uploaded yet", view: self)
            }else{
                self.alertBox(myMsg: "Join Breathe to Perform Pro to unlock all musical selections")
            }
        }else{
            if backgroundMusicTableVw.isHidden == true{
                backgroundMusicTableVw.isHidden = false
            }else{
                backgroundMusicTableVw.isHidden = true
            }
        }
    }
    @objc func readMore_tapped(_ sender: Any) {
        descriptionLbl.sizeToFit()
        readMoreBtn.isHidden = true
        
        linkLbl.isHidden = false
        linkButton.isHidden = false
        linkLbl.frame = CGRect(x: 0, y: descriptionLbl.frame.size.height + descriptionLbl.frame.origin.y + 5, width: 300, height: linkLbl.frame.height)
        linkLbl.text = challengeDetailsDict["challengeUrl"] as? String
        linkLbl.numberOfLines = 0
        linkLbl.textColor = UIColor(red: 35.0/255.0, green: 85.0/255.0, blue: 135.0/255.0, alpha: 1.0)
        linkLbl.textAlignment = .center
        linkLbl.sizeToFit()
        linkLbl.center.x = self.view.center.x
        scrollView.addSubview(linkLbl)
        
        linkButton.frame = CGRect(x: 0, y: descriptionLbl.frame.size.height + descriptionLbl.frame.origin.y + 2, width: 300, height: linkLbl.frame.size.height)
        linkButton.center.x = self.view.center.x
        scrollView.addSubview(linkButton)
        
        var contentRect = CGRect.zero
        for view in scrollView.subviews {
            contentRect = contentRect.union(view.frame)
        }
        scrollView.contentSize.height = contentRect.size.height
    }
    
    func timeString(time:TimeInterval) -> String {
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format: "%02i:%02i", minutes, seconds)
    }
    @objc func updateAudioProgressView(){
        if audioPlayer.isPlaying{
            // Update progress
            progressView.setProgress(Float(audioPlayer.currentTime/audioPlayer.duration), animated: true)
            backwardTimerLbl.text = timeString(time: TimeInterval(audioPlayer.currentTime))
            if backwardTimerLbl.text == timeString(time: TimeInterval(audioPlayer.duration)){
                playImageView.image = UIImage(named: "play")
            }
        }
    }
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        isAudioPlayed = false
        playClicked(self)
    }
    
    @IBAction func playClicked(_ sender: Any) {
        if forwardTimerLbl.text! == "00:00" {
            Alerts.alertBox(Mymsg: "cannot play this audio", view: self)
        }else{
            if isAudioPlayed == false{
                do {
                    try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                    try AVAudioSession.sharedInstance().setActive(true)
                }
                catch {
                }
                audioPlayer.play()
                Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateAudioProgressView), userInfo: nil, repeats: true)
                progressView.setProgress(Float(audioPlayer.currentTime/audioPlayer.duration), animated: false)
                backwardTimerLbl.text = timeString(time: TimeInterval(audioPlayer.currentTime))
                isAudioPlayed = true
                playImageView.image = UIImage(named: "pause")
            }else{
                audioPlayer.stop()
                isAudioPlayed = false
                playImageView.image = UIImage(named: "play")
            }
        }
    }
    //MARK:- VIEW WILL DISAPPEAR
    override func viewWillDisappear(_ animated: Bool) {
        if forwardTimerLbl.text! != "00:00"{
            audioPlayer.stop()
        }
    }
    @IBAction func rewindClicked(_ sender: Any) {
        if forwardTimerLbl.text! != "00:00"{
            var time: TimeInterval = audioPlayer.currentTime
            time -= 5.0 // Go back by 5 seconds
            if playImageView.image == UIImage(named: "pause"){
                audioPlayer.currentTime = time
                backwardTimerLbl.text = timeString(time: TimeInterval(audioPlayer.currentTime))
            }else{
                print("audio paused")
            }
        }
    }
    @IBAction func fastForwardClicked(_ sender: Any) {
        if forwardTimerLbl.text! != "00:00"{
            var time: TimeInterval = audioPlayer.currentTime
            time += 5.0 // Go forward by 5 seconds
            if audioPlayer.currentTime == audioPlayer.duration{
                audioPlayer.play()
                playImageView.image = UIImage(named: "pause")
            }else{
                if playImageView.image == UIImage(named: "pause"){
                    audioPlayer.currentTime = time
                    backwardTimerLbl.text = timeString(time: TimeInterval(audioPlayer.currentTime))
                }
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func markAsCompltedClicked(_ sender: Any) {
        if challengeAlreadyCompleted {
            Alerts.alertBox(Mymsg: "Already completed the challenge", view: self)
            return
        }
        if nextChallengeIdString != ""{
            if NetworkingManager.isConnectedToNetwork(){
                showActivityIndicator()
                //retrieve the token and add it to header
                let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
                let tokenString = "Bearer " + retrievedToken!
                let headers: HTTPHeaders = ["Authorization": tokenString, "Content-Type": "application/json"]
                let statusParams = ["courseId":challengeDetailsDict["courseId"] as? String,"challengeId":nextChallengeIdString] as! [String : String]
                Alamofire.request(addChallengeStatusURL, method: .post, parameters: statusParams, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
//                    hideActivityIndicator()
                    switch response.result {
                    case .success:
                        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "CourseListViewController") as! CourseListViewController
                        self.navigationController?.pushViewController(destVc, animated: true)
                    case .failure(let error):
                        print(error)
//                        hideActivityIndicator()
                    }
                    debugPrint(response)
                }
            }else{
                hideActivityIndicator()
                Alerts.alertBox(Mymsg: "no internet connection", view: self)
            }
        }
        
    }
    func checkIfChallengeCompleted() {
        let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
        let tokenString = "Bearer " + retrievedToken!
        let headers: HTTPHeaders = ["Authorization": tokenString, "Content-Type": "application/json"]
        let courseId = challengeDetailsDict["courseId"] as! String
        let challengeId = challengeDetailsDict["_id"] as! String
        Alamofire.request(getChallengeStatusURL + courseId, headers: headers).responseJSON { response in
//            print(response)
            switch response.result {
            case .success:
                if let result = response.result.value {
                    let JSON = result as! NSArray
                    if JSON.contains(challengeId) && (JSON.lastObject as! String != challengeId) {
                        self.challengeAlreadyCompleted = true
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    @IBAction func backClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
}
