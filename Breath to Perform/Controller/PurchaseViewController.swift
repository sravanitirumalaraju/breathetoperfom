//
//  PurchaseViewController.swift
//  Breath to Perform
//
//  Created by apple on 1/29/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import StoreKit
import Alamofire
import SwiftyJSON

class PurchaseViewController: UIViewController,SKProductsRequestDelegate,SKPaymentTransactionObserver {

    @IBOutlet weak var courseNameLbl: UILabel!
    @IBOutlet weak var courseAmountLbl: UILabel!
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var restoreBtn: UIButton!
    
    var courseDetailsDict = NSDictionary()
    var productsArray: Array<SKProduct?> = []
    var transactionInProgress = false
    var amountTxt = String()
//    var courseSelect = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        courseSelect = UserDefaults.standard.value(forKey: "row") as! Int
        print(courseDetailsDict)
        courseNameLbl.text = courseDetailsDict["courseName"] as? String
        amountTxt = "$"
        courseAmountLbl.text =  amountTxt.appending(courseDetailsDict["courseAmount"] as! String)
        requestProductInfo()
        SKPaymentQueue.default().add(self)
    }
    func requestProductInfo() {
        showActivityIndicator()
        if SKPaymentQueue.canMakePayments() {
            let productRequest = SKProductsRequest(productIdentifiers: Set(["com.BreatheToPerform1"]))
            print(productRequest)
            productRequest.delegate = self
            productRequest.start()
        }
        else {
            print("Cannot perform In App Purchases.")
        }
    }
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        hideActivityIndicator()
        print(response)
        if response.products.count != 0 {
            for product in response.products {
                productsArray.append(product)
            }
        }
        else {
            Alerts.alertBox(Mymsg: "There are no products.", view: self)
        }
        if response.invalidProductIdentifiers.count != 0 {
            print(response.invalidProductIdentifiers.description)
        }
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        showActivityIndicator()
        for transaction in transactions {
            switch transaction.transactionState {
            case SKPaymentTransactionState.purchased:
                print("Transaction completed successfully.")
                SKPaymentQueue.default().finishTransaction(transaction)
                transactionInProgress = false
                purchaseSuccess()
            case SKPaymentTransactionState.failed:
                hideActivityIndicator()
                Alerts.alertBox(Mymsg: "Transaction Failed", view: self)
                SKPaymentQueue.default().finishTransaction(transaction)
                transactionInProgress = false
            case SKPaymentTransactionState.restored:
                hideActivityIndicator()
                SKPaymentQueue.default().finishTransaction(transaction)
            default:
                print(transaction.transactionState.rawValue)
            }
        }
    }
    
    func purchaseSuccess() {
        if NetworkingManager.isConnectedToNetwork() {
            //retrieve the token and add it to header
            let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
            let tokenString = "Bearer " + retrievedToken!
            let headers: HTTPHeaders = ["Authorization": tokenString, "Content-Type": "application/json"]
            let updatePayParams = ["courseId":courseDetailsDict["_id"] as! String,"paidAmount":courseDetailsDict["courseAmount"] as! String]
            let paymentParams = ["payment":updatePayParams]
            print(paymentParams)
            Alamofire.request(updatePaymentURL, method: .post, parameters: paymentParams, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                hideActivityIndicator()
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    Alerts.alertBox(Mymsg: json["message"].string!, view: self)
                case .failure(let error):
                    print(error)
                }
                debugPrint(response)
            }
        }else{
            hideActivityIndicator()
            Alerts.alertBox(Mymsg: "no internet connection", view: self)
        }
    }
    @IBAction func buyClicked(_ sender: Any) {
        print(productsArray as NSArray)
        let payment = SKPayment(product: self.productsArray[0]!)
        SKPaymentQueue.default().add(payment)
        self.transactionInProgress = true
    }
    
    @IBAction func backClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    @IBAction func restoreBtnClicked(_ sender: Any) {
        if (SKPaymentQueue.canMakePayments()) {
            SKPaymentQueue.default().add(self)
            SKPaymentQueue.default().restoreCompletedTransactions()
        }
    }
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        Alerts.alertBox(Mymsg: "You've successfully restored your purchase!", view: self)
    }
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        Alerts.alertBox(Mymsg: "restore transaction failed", view: self)
    }
}
