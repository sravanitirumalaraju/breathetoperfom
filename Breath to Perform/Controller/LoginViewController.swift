//
//  LoginViewController.swift
//  Breath to Perform
//
//  Created by apple on 11/1/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LoginViewController: UIViewController {
    struct Platform {
        static let isSimulator: Bool = {
            var isSim = false
            #if arch(i386) || arch(x86_64)
            isSim = true
            #endif
            return isSim
        }()
    }
    @IBOutlet weak var emailTxtFld: BreatheUITextField!
    @IBOutlet weak var passwordTxtFld: BreatheUITextField!
    @IBOutlet weak var loginButton: BreatheUIButton!
    
    @IBOutlet weak var BreatheLogo: UIImageView!
    var blurView = UIView()
    var popUpView = UIView()
    let enterEmailTxtFld = BreatheUITextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        blurView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        blurView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.view.addSubview(blurView)
        blurView.isHidden = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        view.addGestureRecognizer(tap)
        view.isUserInteractionEnabled = true
        
        BreatheLogo.layer.cornerRadius = BreatheLogo.frame.size.height/2
        BreatheLogo.clipsToBounds = true
    }
    //MARK:- HANDLE TAP GESTURE
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func forgotPassClicked(_ sender: Any) {
        blurView.isHidden = false
        popUpView.isHidden = false
        enterEmailTxtFld.text = ""
        
        popUpView.frame = CGRect(x: 0, y: 0, width: 300, height: 160)
        popUpView.center = self.blurView.center
        popUpView.backgroundColor = UIColor.white
        popUpView.layer.cornerRadius = 10
        popUpView.clipsToBounds = true
        blurView.addSubview(popUpView)
        
        let cancelBtn = UIButton.init(frame: CGRect(x: popUpView.frame.size.width-40, y: 10, width: 30, height: 30))
        cancelBtn.setImage(UIImage(named:"cross_blue"), for: .normal)
        cancelBtn.addTarget(self, action: #selector(cancelClicked), for: .touchUpInside)
        popUpView.addSubview(cancelBtn)
        
        enterEmailTxtFld.frame = CGRect(x: 25, y: 45, width: 250, height: 40)
        enterEmailTxtFld.placeholder = "Enter Email"
        enterEmailTxtFld.textAlignment = .center
        enterEmailTxtFld.keyboardType = .emailAddress
        enterEmailTxtFld.autocapitalizationType = .none
        enterEmailTxtFld.autocorrectionType = .no
//        enterEmailTxtFld.delegate = self
        popUpView.addSubview(enterEmailTxtFld)
        
        let submitBtn = UIButton.init(frame: CGRect(x: 50, y: enterEmailTxtFld.frame.size.height + enterEmailTxtFld.frame.origin.y + 20, width: 200, height: 40))
        submitBtn.setTitle("Submit", for: .normal)
        submitBtn.layer.cornerRadius = submitBtn.frame.size.height/2
        submitBtn.backgroundColor = UIColor(red: 35.0/255.0, green: 85.0/255.0, blue: 135.0/255.0, alpha: 1.0)
        submitBtn.addTarget(self, action: #selector(submitClicked), for: .touchUpInside)
        popUpView.addSubview(submitBtn)
    }
    //MARK:- CANCEL CLICKED
    @objc func cancelClicked(){
        popUpView.isHidden = true
        blurView.isHidden = true
    }
    @objc func submitClicked(){
        if NetworkingManager.isConnectedToNetwork(){
            showActivityIndicator()
            let forgotParams = ["email":enterEmailTxtFld.text!]
            let userHeaders: HTTPHeaders = ["Content-Type": "application/json"]
            Alamofire.request(forgetPasswordURL, method: .put, parameters: forgotParams, encoding: JSONEncoding.default, headers: userHeaders).responseJSON { response in
                print(response)
                hideActivityIndicator()
                self.view.endEditing(true)
                self.blurView.isHidden = true
                switch(response.result) {
                case .success(let value):
                    let json = JSON(value)
                    if response.response?.statusCode == 200{
                        Alerts.alertBox(Mymsg: "Password has been sent to your email please check your email", view: self)
                        self.popUpView.isHidden = true
                        self.blurView.isHidden = true
                    }else{
                        Alerts.alertBox(Mymsg: json["message"].string!, view: self)
                    }
                case .failure :
                    print(response.result.error!)
                    break
                }
            }
        }else{
            Alerts.alertBox(Mymsg: "No internet connection", view: self)
        }
    }
    
    @IBAction func loginBtnClicked(_ sender: Any) {
        if NetworkingManager.isConnectedToNetwork(){
            if emailTxtFld.text! == "" && passwordTxtFld.text! == ""{
                Alerts.alertBox(Mymsg: "please enter emailId & password", view: self)
            }else if emailTxtFld.text! == "" {
                Alerts.alertBox(Mymsg: "please enter emailId", view: self)
            }else if passwordTxtFld.text! == ""{
                Alerts.alertBox(Mymsg: "please enter password", view: self)
            }else{
                showActivityIndicator()
                let isEmailAddressValid = isValidEmailAddress(emailAddressString: emailTxtFld.text!)
                if isEmailAddressValid{
                    let loginParams = ["emailId":emailTxtFld.text!,"password":passwordTxtFld.text!]
                    let userHeaders: HTTPHeaders = ["Content-Type": "application/json"]
                    Alamofire.request(loginURL, method: .post, parameters: loginParams, encoding: JSONEncoding.default, headers: userHeaders).responseJSON { response in
                        print(response)
                        switch(response.result) {
                        case .success(let value):
                            let json = JSON(value)
                            print(json)
                            if response.response?.statusCode == 200{
                                print("success")
                                self.performSegue(withIdentifier: "GoToCoursesList", sender: self)
                                UserDefaults.standard.set(self.emailTxtFld.text!, forKey: "emailId")
                                UserDefaults.standard.set(json["userId"].string!, forKey: "userId")
                                UserDefaults.standard.synchronize()
                                let stringValue = json["token"].string!
                                print("the value is : \(stringValue)")
                                UserDefaults.standard.setValue(stringValue, forKey: "token")
                                UserDefaults.standard.synchronize()
                                self.appRegisteration()
                            }else{
                                if let sucessValue = json["message"].string{
                                    if sucessValue == "NO_EMAIL"{
                                        Alerts.alertBox(Mymsg: "You are not Registered user with this email ", view: self)
                                    }else{
                                        Alerts.alertBox(Mymsg: "You entered wrong password", view: self)
                                    }
                                }
                            }
                            hideActivityIndicator()
                        case .failure(_):
                            hideActivityIndicator()
                            print(response.result.error!)
                            break
                        }
                        debugPrint(response)
                    }
                }else{
                    Alerts.alertBox(Mymsg: "Please enter valid email id", view: self)
                }
            }
        }else{
            print("no network")
            Alerts.alertBox(Mymsg: "no internet connection", view: self)
        }
    }

    func isValidEmailAddress(emailAddressString: String) -> Bool {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            if results.count == 0{
                returnValue = false
            }
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
    //MARK:- APP REGISTERATION
    @objc func appRegisteration(){
        if Platform.isSimulator == false{
            if let FCMToken: String = UserDefaults.standard.value(forKey: "FCM_Token") as? String{
                if FCMToken != "" {
                    let registerationId = UserDefaults.standard.value(forKey: "FCM_Token") as! String
                    let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
                    let tokenString = "Bearer " + retrievedToken!
                    let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
                    let registerParams = ["registrationId":registerationId]
                    Alamofire.request(appRegisterationURL, method: .post, parameters: registerParams, encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
                        switch (response.result) {
                        case .success:
                            print(response.result.value!)
                        case .failure:
                            print(response.result.error!)
                            break
                        }
                        debugPrint(response)
                    }
                }
            }
        }
    }
}
