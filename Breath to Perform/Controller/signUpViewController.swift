//
//  signUpViewController.swift
//  Breath to Perform
//
//  Created by apple on 11/1/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class signUpViewController: UIViewController {
    struct Platform {
        static let isSimulator: Bool = {
            var isSim = false
            #if arch(i386) || arch(x86_64)
            isSim = true
            #endif
            return isSim
        }()
    }
    @IBOutlet weak var userNameTxtFld: BreatheUITextField!
    @IBOutlet weak var emailTxtFld: BreatheUITextField!
    @IBOutlet weak var passwordTxtFld: BreatheUITextField!
    @IBOutlet weak var mobileNoTxtFld: BreatheUITextField!
    @IBOutlet weak var signUpBtn: BreatheUIButton!
    @IBOutlet weak var breatheLogo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        view.addGestureRecognizer(tap)
        view.isUserInteractionEnabled = true
        
        breatheLogo.layer.cornerRadius = breatheLogo.frame.size.height/2
        breatheLogo.clipsToBounds = true

    }
    //MARK:- HANDLE TAP GESTURE
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func loginInsteadTapped(_ sender: Any) {
        let destVc = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        navigationController?.pushViewController(destVc, animated: true)
    }
    @IBAction func signUpClicked(_ sender: Any) {
        if NetworkingManager.isConnectedToNetwork(){
            if userNameTxtFld.text == "" && emailTxtFld.text == "" && passwordTxtFld.text == "" && mobileNoTxtFld.text == ""{
                Alerts.alertBox(Mymsg: "Please enter all fields", view: self)
            }else if userNameTxtFld.text == ""{
                Alerts.alertBox(Mymsg: "Please enter userName", view: self)
            }else if emailTxtFld.text == ""{
                Alerts.alertBox(Mymsg: "Please enter email Address", view: self)
            }else if passwordTxtFld.text == ""{
                Alerts.alertBox(Mymsg: "Please enter password", view: self)
            }else if mobileNoTxtFld.text == ""{
                Alerts.alertBox(Mymsg: "Please enter mobile number", view: self)
            }else{
                showActivityIndicator()
                let isEmailAddressValid = isValidEmailAddress(emailAddressString: emailTxtFld.text!)
                if isEmailAddressValid{
                    let signUpParams = ["userName":"\(userNameTxtFld.text!)","emailId":"\(emailTxtFld.text!)","mobileNumber":"\(mobileNoTxtFld.text!)","password":"\(passwordTxtFld.text!)"]
                    print(signUpParams)
                    let userHeaders: HTTPHeaders = ["Content-Type": "application/json"]
                    Alamofire.request(signUpURL, method: HTTPMethod.post, parameters: signUpParams,encoding: JSONEncoding.default, headers: userHeaders).responseJSON { response in
                        print(response)
                        switch(response.result) {
                        case .success(let value):
                            let json = JSON(value)
                            print(json)
                            if response.response?.statusCode == 200{
                                print("sucess")
                                if let sucessValue = json["success"].string{
                                    Alerts.alertBox(Mymsg: sucessValue, view: self)
                                }else{
                                    let destVc = self.storyboard?.instantiateViewController(withIdentifier: "CourseListViewController") as! CourseListViewController
                                    self.navigationController?.pushViewController(destVc, animated: true)
                                    UserDefaults.standard.set(self.emailTxtFld.text!, forKey: "emailId")
                                    let stringValue = json["token"].string!
                                    print("the value is : \(stringValue)")
                                    UserDefaults.standard.setValue(stringValue, forKey: "token")
                                    UserDefaults.standard.set(json["userId"].string!, forKey: "userId")
                                    UserDefaults.standard.synchronize()
                                    self.appRegisteration()
                                }
                            }
                            hideActivityIndicator()
                        case .failure(_):
                            hideActivityIndicator()
                            print(response.result.error!)
                            break
                        }
                        debugPrint(response)
                    }
                }else{
                    Alerts.alertBox(Mymsg: "Please enter valid email id", view: self)
                }
            }
        }else{
            Alerts.alertBox(Mymsg: "No internet connection", view: self)
        }
    }
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            if results.count == 0{
                returnValue = false
            }
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
    
    //MARK:- APP REGISTERATION
    @objc func appRegisteration(){
        if Platform.isSimulator == false{
            if let FCMToken: String = UserDefaults.standard.value(forKey: "FCM_Token") as? String{
                if FCMToken != "" {
                    let registerationId = UserDefaults.standard.value(forKey: "FCM_Token") as! String
                    let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
                    let tokenString = "Bearer " + retrievedToken!
                    let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
                    let registerParams = ["registrationId":registerationId]
                    Alamofire.request(appRegisterationURL, method: .post, parameters: registerParams, encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
                        switch (response.result) {
                        case .success:
                            print(response.result.value!)
                        case .failure:
                            print(response.result.error!)
                            break
                        }
                        debugPrint(response)
                    }
                }
            }
        }
    }
}
