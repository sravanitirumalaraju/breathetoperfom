//
//  CourseListViewController.swift
//  Breath to Perform
//
//  Created by apple on 11/1/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CourseListViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var challengesCollectionVw: UICollectionView!
    @IBOutlet weak var courseTitleLbl: BreatheUILabel!
    @IBOutlet weak var courseListPickerVw: UIPickerView!
    @IBOutlet weak var selectedCourseLbl: BreatheUIButton!
    
    var challengeItems = [Int]()
    var allCoursesArray = [String]()
    var purchaseArray = NSMutableArray()
    var allCoursesId = [String]()
    var CourseRow = Int()
    var courseIdSelected = String()
    var allChallengesArray = NSMutableArray()
    var noDataString = String()
    var selectedChallengesArray = NSArray()
    var selectedChallengeId = String()
    var isChallengeFinish = Bool()
    var isSelected = Int()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.courseListPickerVw.isHidden = true
    }

    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        isChallengeFinish = false
        if NetworkingManager.isConnectedToNetwork(){
            showActivityIndicator()
            let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
            let tokenString = "Bearer " + retrievedToken!
            let headers: HTTPHeaders = ["Authorization": tokenString, "Content-Type": "application/json"]
            Alamofire.request(getAllCourseListURL, headers: headers).responseJSON { response in
                switch response.result {
                case .success:
                    if let result = response.result.value {
                        self.allCoursesArray.removeAll()
                        self.purchaseArray.removeAllObjects()
                        let coursesResultArray = result as! NSArray
                        for value in coursesResultArray{
                            let valueDict = value as! NSDictionary
                            if (valueDict["makeThisCourse"] as! String) == "SHOW"{
                                self.purchaseArray.add(valueDict)
                                self.allCoursesArray.append(valueDict["courseName"] as! String)
                                self.allCoursesId.append(valueDict["_id"] as! String)
                            }
                        }
                        if let Id = UserDefaults.standard.value(forKey: "courseId") as? String{
//                            self.courseTitleLbl.text = UserDefaults.standard.value(forKey: "courseName") as? String
                            self.courseIdSelected = Id
                            self.CourseRow = UserDefaults.standard.value(forKey: "row") as! Int
//                            print(self.CourseRow)
                        }else{
//                            self.courseTitleLbl.text = self.allCoursesArray[0]
                            self.courseIdSelected = self.allCoursesId[0]
                            self.CourseRow = 0
                        }
//                        self.getChallengeList(courseId: self.courseIdSelected)
                        self.challengesDisplay(courseId: self.courseIdSelected)
                    }
                case .failure(let error):
                    print(error)
                    hideActivityIndicator()
                }
            }
        }else{
            hideActivityIndicator()
            Alerts.alertBox(Mymsg: "no internet connection", view: self)
        }
    }
//    //MARK:- CHALLENGE STATUS
    func challengesDisplay(courseId:String){
        let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
        let tokenString = "Bearer " + retrievedToken!
        let headers: HTTPHeaders = ["Authorization": tokenString, "Content-Type": "application/json"]
//        showActivityIndicator()
        Alamofire.request(getChallengeStatusURL + courseId, headers: headers).responseJSON { response in
//            print(response)
            switch response.result {
            case .success:
                if let result = response.result.value {
                    let JSON = result as! NSArray
                    self.selectedChallengesArray = JSON
                }
                self.getChallengeList(courseId: courseId)
            case .failure(let error):
                print(error)
            }
        }
    }
    //MARK:- CHALLENGE LIST
    func getChallengeList(courseId:String){
        let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
        let tokenString = "Bearer " + retrievedToken!
        let headers: HTTPHeaders = ["Authorization": tokenString, "Content-Type": "application/json"]
        Alamofire.request(getAllChallengesListURL, headers: headers).responseJSON { response in
//            print(response)
            switch(response.result) {
            case .success:
                if let result = response.result.value {
                    let JSON = result as! NSArray
                    self.challengeItems.removeAll()
                    self.allChallengesArray.removeAllObjects()
                    for (index, value) in JSON.enumerated(){
                        let dict = value as! NSDictionary
                        if let val = dict["message"] {
                            self.noDataString.removeAll()
                            self.noDataString = val as! String
                        }else{
                            self.allChallengesArray.add(value)
                            if (dict["challengeName"] as! String) != ""{
                                let challengeInt = Int(index)
                                self.challengeItems.append(challengeInt + 1)
                            }
                        }
                    }
                    self.challengesCollectionVw.delegate = self
                    self.challengesCollectionVw.dataSource = self
                    self.challengesCollectionVw.reloadData()
                    self.fetchPaymentDetails(courseId:courseId)
                }
            case .failure(_):
                print(response.result.error!)
                break
            }
        }
    }

//    //MARK:- PAYMENT VIEW
    func fetchPaymentDetails(courseId:String){
        if NetworkingManager.isConnectedToNetwork(){
            let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
            let tokenString = "Bearer " + retrievedToken!
            let headers: HTTPHeaders = ["Authorization": tokenString, "Content-Type": "application/json"]
            Alamofire.request(getUserPaymentDetailsURL + courseId, headers: headers).responseJSON {
                response in
//                print(response)
//                hideActivityIndicator()
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
//                    print(json)
                    if json["message"].string! == "Course Paid already"{
                        UserDefaults.standard.set("YES", forKey: "payUpdate")
                        UserDefaults.standard.synchronize()
                    }
                    else{
                        UserDefaults.standard.set("NO", forKey: "payUpdate")
                        UserDefaults.standard.synchronize()
                        let courseFinishDict = self.purchaseArray[self.CourseRow] as! NSDictionary
//                        print(courseFinishDict)
                        let rowSelect = (UserDefaults.standard.value(forKey: "\(self.CourseRow)"))
                        if rowSelect == nil{
                            UserDefaults.standard.set(self.CourseRow, forKey: "\(self.CourseRow)")
                            UserDefaults.standard.synchronize()
//                            if courseFinishDict["type"] as! String == "FREE"{
//                                Alerts.alertBox(Mymsg: "This Course is FREE!", view: self)
//                            }else if courseFinishDict["type"] as! String == "PAID"{
//                                Alerts.alertBox(Mymsg: "This is a Paid Course ($7.99)", view: self)
//                            }else if courseFinishDict["type"] as! String == "TRIAL"{
//                                Alerts.alertBox(Mymsg: "This Course has a FREE trial period of 3 Days", view: self)
//                            }
                            
                            if courseFinishDict["type"] as! String == "FREE"{
                                self.alertBox(myMsg: "This Course is FREE!")
                            }else if courseFinishDict["type"] as! String == "PAID"{
                                self.courseAlertBox(myMsg: "This is a Paid Course ($7.99)")
                            }else if courseFinishDict["type"] as! String == "TRIAL"{
                                self.trailAlertBox(myMsg: "This Course has a FREE trial period of 3 Days")
                            }
                            
                            
                        }
                        if courseFinishDict["type"] as! String == "FREE"{
                            UserDefaults.standard.set("FREE", forKey: "type")
                        }else if courseFinishDict["type"] as! String == "PAID"{
                            UserDefaults.standard.set("PAID", forKey: "type")
                        }else if courseFinishDict["type"] as! String == "TRIAL"{
                            UserDefaults.standard.set("TRIAL", forKey: "type")
                        }
                        UserDefaults.standard.synchronize()
                    }
                    hideActivityIndicator()
                case .failure(let error):
                    hideActivityIndicator()
                    print(error)
                }
            }
        }else{
            hideActivityIndicator()
            Alerts.alertBox(Mymsg: "no internet connection", view: self)
        }
    }
    //MARK:- COLLECTIONVIEW DELEGATE & DATASOURCE
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if challengeItems.count == 0{
            return 1
        }else{
            return challengeItems.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        if challengeItems.count == 0{
            return CGSize(width: CGFloat(200), height: CGFloat(200))
        }else{
            return CGSize(width: CGFloat((challengesCollectionVw.frame.size.width)/8), height: CGFloat(challengesCollectionVw.frame.size.width)/8)
        }
//        return CGSize(width: 60, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ChallengesCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier:"ChallengesCollectionCell", for: indexPath as IndexPath) as! ChallengesCollectionCell
        if challengeItems.count == 0{
            cell.noDataLabel.isHidden = false
            cell.itemTitle.text = ""
            cell.containerView.backgroundColor = UIColor.white
            cell.noDataLabel.text = "No challenges Found"
            cell.noDataLabel.textColor = UIColor(red: 35.0/255.0, green: 85.0/255.0, blue: 135.0/255.0, alpha: 1.0)
            cell.noDataLabel.textAlignment = .center
        }else{
            cell.noDataLabel.isHidden = true
            cell.noDataLabel.text = ""
            cell.itemTitle.text = String(challengeItems[indexPath.row])
            cell.itemTitle.textColor = UIColor(red: 35.0/255.0, green: 85.0/255.0, blue: 135.0/255.0, alpha: 1.0)
            cell.containerView.backgroundColor =  UIColor.white

            if selectedChallengesArray.count == 0{
                if allCoursesId[0] == courseIdSelected{
                    if indexPath.row == 0{
                        cell.itemTitle.textColor = UIColor.white
                        cell.containerView.backgroundColor =  UIColor(red: 35.0/255.0, green: 85.0/255.0, blue: 135.0/255.0, alpha: 1.0)
                        let indDict = allChallengesArray[0] as! NSDictionary
                        selectedChallengesArray = [indDict["_id"] as! String]
                    }
                }else{
                    if indexPath.row == 0{
                        cell.itemTitle.textColor = UIColor.white
                        cell.containerView.backgroundColor =  UIColor(red: 35.0/255.0, green: 85.0/255.0, blue: 135.0/255.0, alpha: 1.0)
                        let indDict = allChallengesArray[0] as! NSDictionary
                        selectedChallengesArray = [indDict["_id"] as! String]
                    }
                }
            }else{
                for eachChallenge in selectedChallengesArray{
                    let indDict = allChallengesArray[indexPath.row] as! NSDictionary
                    if indDict["_id"] as! String == eachChallenge as! String{
                        cell.itemTitle.textColor = UIColor.white
                        cell.containerView.backgroundColor =  UIColor(red: 35.0/255.0, green: 85.0/255.0, blue: 135.0/255.0, alpha: 1.0)
                        break
                    }
                }
            }
        }
        cell.containerView.layer.borderColor = UIColor.gray.cgColor
        cell.containerView.layer.borderWidth = 1
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            if challengeItems.count != 0{
                isSelected = indexPath.row
                if (UserDefaults.standard.value(forKey: "payUpdate") as? String == "YES") || (UserDefaults.standard.value(forKey: "type") as? String == "FREE") || (UserDefaults.standard.value(forKey: "type") as? String == "TRIAL" && isSelected < 3){
                    let dict = allChallengesArray[isSelected] as! NSDictionary
                    for indChall in selectedChallengesArray{
                        if dict["_id"] as! String == indChall as! String{
                            selectedChallengeId = indChall as! String
                            isChallengeFinish = true
                            let purchaseDict = self.purchaseArray[self.CourseRow] as! NSDictionary
                            UserDefaults.standard.set(purchaseDict["_id"] as? String, forKey: "courseId")
                            UserDefaults.standard.synchronize()
                            UserDefaults.standard.set(purchaseDict["courseName"] as? String, forKey: "courseName")
                            UserDefaults.standard.synchronize()
                            UserDefaults.standard.set(self.CourseRow, forKey: "row")
                            UserDefaults.standard.synchronize()
                            DispatchQueue.main.async() { () -> Void in
                                let nextScene = self.storyboard?.instantiateViewController(withIdentifier: "ChallengeViewController") as! ChallengeViewController
                                nextScene.challengeDetailsDict = self.allChallengesArray[self.isSelected] as! NSDictionary
                                nextScene.numberLabelString = String(self.isSelected + 1)
                                nextScene.purchaseInfoDict = self.purchaseArray[self.CourseRow] as! NSDictionary
                                if self.isSelected + 1 != self.allChallengesArray.count{
                                    let nextIdDict = self.allChallengesArray[self.isSelected + 1] as! NSDictionary
                                    let nextIdString = nextIdDict["_id"] as? String
                                    nextScene.nextChallengeIdString = nextIdString!
                                }else{
                                    let nextIdDict = self.allChallengesArray[self.isSelected] as! NSDictionary
                                    let nextIdString = nextIdDict["_id"] as? String
                                    nextScene.nextChallengeIdString = nextIdString!
                                }
                                let unlockedIdString = self.selectedChallengesArray[self.selectedChallengesArray.count - 1]
                                nextScene.unlockedChallengeIdString = unlockedIdString as! String
                                
                                if self.allCoursesId[0] == self.courseIdSelected{
                                    if self.isSelected == 0{
                                        self.addingFirstChallenge()
                                    }
                                }else{
                                    if self.isSelected == 0{
                                        self.addingFirstChallenge()
                                    }
                                }
                                self.navigationController?.pushViewController(nextScene, animated: true)
                            }
                            break
                        }
                    }
                    if isChallengeFinish == false{
                        Alerts.alertBox(Mymsg: "Complete the previous challenge", view: self)
                    }
                }else{
                    self.courseAlertBox(myMsg: "This is a Paid Course ($7.99)")
//                    Alerts.alertBox(Mymsg: "This is a Paid Course ($7.99)", view: self)
                }
            }
    }
    //MARK:- ALERTBOX
    func alertBox (myMsg: String){
        let alert = UIAlertController(title: "", message: myMsg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { UIAlertAction in
            let purchaseDict = self.purchaseArray[self.CourseRow] as! NSDictionary
//            print(purchaseDict)
            UserDefaults.standard.set(purchaseDict["_id"] as? String, forKey: "courseId")
            UserDefaults.standard.synchronize()
            UserDefaults.standard.set(purchaseDict["courseName"] as? String, forKey: "courseName")
            UserDefaults.standard.synchronize()
            UserDefaults.standard.set(self.CourseRow, forKey: "row")
            UserDefaults.standard.synchronize()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    //MARK:- ALERTS FOR COURSES
    func courseAlertBox (myMsg: String){
        let alert = UIAlertController(title: "", message: myMsg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "PAY", style: .default, handler: { UIAlertAction in
            let destVc = self.storyboard?.instantiateViewController(withIdentifier: "PurchaseViewController") as! PurchaseViewController
            destVc.courseDetailsDict = self.purchaseArray[self.CourseRow] as! NSDictionary
            self.navigationController?.pushViewController(destVc, animated: true)
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
        let purchaseDict = self.purchaseArray[self.CourseRow] as! NSDictionary
        print(purchaseDict)
        UserDefaults.standard.set(purchaseDict["_id"] as? String, forKey: "courseId")
        UserDefaults.standard.synchronize()
        UserDefaults.standard.set(purchaseDict["courseName"] as? String, forKey: "courseName")
        UserDefaults.standard.synchronize()
        UserDefaults.standard.set(self.CourseRow, forKey: "row")
        UserDefaults.standard.synchronize()
        
        
    }
    
    func trailAlertBox (myMsg: String){
        let alert = UIAlertController(title: "", message: myMsg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "START", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        let purchaseDict = self.purchaseArray[self.CourseRow] as! NSDictionary
//        print(purchaseDict)
        UserDefaults.standard.set(purchaseDict["_id"] as? String, forKey: "courseId")
        UserDefaults.standard.synchronize()
        UserDefaults.standard.set(purchaseDict["courseName"] as? String, forKey: "courseName")
        UserDefaults.standard.synchronize()
        UserDefaults.standard.set(self.CourseRow, forKey: "row")
        UserDefaults.standard.synchronize()
    }
    func addingFirstChallenge(){
        let retrievedToken: String? = UserDefaults.standard.value(forKey: "token") as? String
        let userId = UserDefaults.standard.value(forKey: "userId") as? String
        let tokenString = "Bearer " + retrievedToken!
        let headers: HTTPHeaders = ["Authorization": tokenString, "Content-Type": "application/json"]
        var statusParams = [String:Any]()
        let statusDict = allChallengesArray[isSelected] as! NSDictionary
        statusParams = ["userId":userId!,"courseId":statusDict["courseId"] as? String as Any,"challengeId":statusDict["_id"] as! String]
        Alamofire.request(addChallengeStatusURL, method: .post, parameters: statusParams, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            switch response.result {
            case .success:
                break
            case .failure(let error):
                print(error)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func selectACourseClicked(_ sender: Any) {
//        self.courseListPickerVw.isHidden = false
//        self.courseListPickerVw.delegate = self
//        self.courseListPickerVw.dataSource = self
    }
    //MARK:- PICKER VIEW DELEGATE & DATASOURCE
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return allCoursesArray.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row == 0{
            self.selectedCourseLbl.setTitle(allCoursesArray[row], for: .normal)
            pickerView.isHidden = true
            courseIdSelected = allCoursesId[row]
            CourseRow = row
            challengesDisplay(courseId: courseIdSelected)
        }else{
            print("courseFinish")
            self.selectedCourseLbl.setTitle(allCoursesArray[row], for: .normal)
            pickerView.isHidden = true
            courseIdSelected = allCoursesId[row]
            CourseRow = row
            challengesDisplay(courseId: courseIdSelected)
        }
        let purchaseDict = self.purchaseArray[self.CourseRow] as! NSDictionary
//        print(purchaseDict)
        UserDefaults.standard.set(purchaseDict["_id"] as? String, forKey: "courseId")
        UserDefaults.standard.synchronize()
        UserDefaults.standard.set(purchaseDict["courseName"] as? String, forKey: "courseName")
        UserDefaults.standard.synchronize()
        UserDefaults.standard.set(self.CourseRow, forKey: "row")
        UserDefaults.standard.synchronize()
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let attributedString = NSAttributedString(string: allCoursesArray[row], attributes: [NSAttributedStringKey.foregroundColor :
            UIColor(red: 35.0/255.0, green: 85.0/255.0, blue: 135.0/255.0, alpha: 1.0)])
        return attributedString
    }
}
