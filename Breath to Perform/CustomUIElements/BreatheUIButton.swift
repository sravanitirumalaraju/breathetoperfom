//
//  BreatheUIButton.swift
//  Breath to Perform
//
//  Created by apple on 11/1/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation
import UIKit

class BreatheUIButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        layer.cornerRadius = 5.0
        clipsToBounds = true
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        changeFontName()
        
        
    }
    func changeFontName()
    {
        
        self.titleLabel!.font = UIFont(name: "Helvetica", size: (titleLabel?.font.pointSize)!)!
    }
    
}
