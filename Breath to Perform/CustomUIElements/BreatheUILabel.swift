//
//  BreatheUILabel.swift
//  Breath to Perform
//
//  Created by apple on 11/1/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation
import UIKit

class BreatheUILabel: UILabel {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        changeFontName()
        numberOfLines = 0
        lineBreakMode = NSLineBreakMode.byWordWrapping
        adjustsFontSizeToFitWidth =  true
        if #available(iOS 9.0, *) {
            allowsDefaultTighteningForTruncation =  true
        } else {
            // Fallback on earlier versions
        }
        sizeToFit()
    }
    
    func changeFontName()
    {
        self.font = UIFont(name: "Helvetica", size: self.font.pointSize)
    }
}
