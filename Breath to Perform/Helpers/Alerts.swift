//
//  Alerts.swift
//  Breath to Perform
//
//  Created by apple on 12/26/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation
import UIKit

class Alerts{
    
    static func alertBox (Mymsg:String, view: UIViewController){
        let alert = UIAlertController(title: "", message: Mymsg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        view.present(alert, animated: true, completion: nil)
    }
    
}
