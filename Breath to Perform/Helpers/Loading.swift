//
//  Loading.swift
//  Breath to Perform
//
//  Created by apple on 12/26/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation
import NVActivityIndicatorView
import UIKit

let activityData = ActivityData()

func showActivityIndicator(){
    NVActivityIndicatorView.DEFAULT_COLOR = UIColor.white
    NVActivityIndicatorView.DEFAULT_BLOCKER_MESSAGE = "loading.."
    NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.4)
    NVActivityIndicatorView.DEFAULT_TYPE = .lineSpinFadeLoader
    NVActivityIndicatorView.DEFAULT_BLOCKER_SIZE = CGSize(width: 25, height: 25)
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
}

func hideActivityIndicator(){
    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
}
