//
//  MusicTableViewCell.swift
//  Breath to Perform
//
//  Created by apple on 12/11/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class MusicTableViewCell: UITableViewCell {

    @IBOutlet weak var backgroundNameLbl: UILabel!
    
    @IBOutlet weak var lockBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
