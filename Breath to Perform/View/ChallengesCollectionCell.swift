//
//  ChallengesCollectionCell.swift
//  Breath to Perform
//
//  Created by apple on 11/1/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class ChallengesCollectionCell: UICollectionViewCell {
    
    @IBOutlet var containerView: UIView!
    @IBOutlet var itemTitle: BreatheUILabel!
    @IBOutlet var noDataLabel: UILabel!
    
    override func awakeFromNib() {
//        containerView.makeCircular()
        containerView.layer.cornerRadius = 30
        containerView.clipsToBounds = true
    }
    
    func showSelectionOnCell() {
        containerView.backgroundColor =  UIColor.orange
        itemTitle.textColor = UIColor.white
    }
    
}
